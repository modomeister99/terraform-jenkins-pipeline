terraform {
  # required_version = ">= 1.3.8"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.55.0"
    }

  }
}



provider "aws" {
  #region = "us-east-1"
  #profile = "default"
}

#Create an S3 bucket and DynamoDB table before running
terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket         = "terraform-statefilexxx7"
    key            = "jjtech/terraform.tfstate"
    region         = "us-east-1"

    # Replace this with your DynamoDB table name!
    dynamodb_table = "backend-locks" #the partitionkey must be "LockID"
  }
 }




